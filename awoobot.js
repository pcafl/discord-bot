'use strict';
const Discord = require('discord.js');
const awoobot = new Discord.Client();
const fs = require('fs');
const fetchJSON = require('node-fetch-json');
const permissions = require('./permissions.js');

global.commands = {};

var boardCache = new Map();
var fourchanBlacklist = ['f'].sort();
var answersList = ['It is certain', 'It is decidedly so', 'Without a doubt', 'Yes definitely', 'You may rely on it',
	'As I see it, yes', 'Most likely', 'Outlook good', 'Yes', 'Signs point to yes', 'Reply hazy, try again', 'Ask again later', 'Better not tell you now',
	'Concentrate and ask again', 'Dont count on it', 'My reply is no', 'My sources say no', 'Outlook not so good', 'Very doubtful'];
var fortunes = ['Reply hazy, try again', 'Excellent Luck', 'Good Luck', 'Average Luck', 'Bad Luck', 'Good news will come to you by mail', '（　´_ゝ`）ﾌｰﾝ',
	'ｷﾀ━━━━━━(ﾟ∀ﾟ)━━━━━━ !!!!', 'You will meet a dark, handsome stranger', 'Better not tell you now', 'Outlook good', 'Very Bad Luck', 'Godly Luck'];

var fileQueue = []; //uri array
var voiceDispatcher;
var voiceConnection;


function playQueue(msg, filename)
{
	console.log('voiceConnection could be found, adding filename to queue');
	fileQueue = [filename].concat(fileQueue);
	msg.channel.send('Added to queue.');
	if(!msg.guild)
	{
		msg.channel.send('You need to send this from a server text channel and not in a DM');
		return;
	}
	if(msg.member.voiceChannel)
	{
		console.log('Message author is in a voice channel');
		msg.member.voiceChannel.join()
			.then((connection) => {
				console.log();
				voiceConnection = connection;
				nextQueue(msg);
			});
	} else
	{
		msg.reply('You need to be in a voice channel first.');
	}

}

function nextQueue(msg)
{
	if(fileQueue.length == 0)
	{
		msg.channel.send('Reached the end of the queue!');
		voiceDispatcher = undefined;
		voiceConnection.channel.leave();
		voiceConnection = undefined;
		return;
	}
	let filename = fileQueue.pop();
	voiceDispatcher = voiceConnection.playFile('./' + filename);
	voiceDispatcher.setVolume(1.0);

	voiceDispatcher.on('error', (e) =>
	{
		msg.channel.send('Error! Trying to continue.\n' + e);
		nextQueue(msg);
	});

	voiceDispatcher.on('end', () =>
	{
		voiceDispatcher = undefined;
		nextQueue(msg);
	});

	msg.channel.send('Now playing: ' + filename);
}

Date.now = function()
{
	return Math.trunc(new Date().getTime()/1000);
};

function getRandomIntInclusive(min, max)
{
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}

function saveCache()
{
	//iterera genom brädena
	var JSONobj = [];
	var index = 0;
	console.log('Compiling cache.');
	boardCache.forEach((val, key, map) =>
	{
		JSONobj.push([]);
		JSONobj[index].push(key);
		val.forEach((boardval, boardkey, boardmap) =>
		{
			if(typeof(boardkey) == 'number')
			{
				JSONobj[index].push(boardkey);
				JSONobj[index].push(boardval.get('files'));
				JSONobj[index].push(boardval.get('externalModified'));
				JSONobj[index].push(boardval.get('lastUpdated'));
			} else
			{
				JSONobj[index].push(boardval);
			}
		});
		index++;
	});
	//console.log(JSON.stringify(JSONobj));
	console.log('Writing cache to disk.');
	fs.writeFileSync('./cache', JSON.stringify(JSONobj));
	console.log('File written.');
}

function loadCache()
{
	var filestring;
	try
	{
		filestring = fs.readFileSync('./cache', 'utf8');
	} catch(err)
	{
		console.log('Error reading cache file.');
		return;
	}
	var JSONobj = JSON.parse(filestring);
	//console.log(JSONobj);
	console.log('Importing locally saved cache...');
	for(var boardIndex = 0; boardIndex < JSONobj.length; boardIndex++) // iterera först genom brädena
	{
		// layouten ser ut som följande för 1 bräde ["brädesnamn", "lastupdate", ["blacklist" array] (, trådnummer, filarray[], externalModified, lastupdate)]
		boardCache.set(JSONobj[boardIndex].slice(0,1)[0], new Map());
		var currentCache = boardCache.get(JSONobj[boardIndex].slice(0,1)[0]);
		currentCache.set('lastUpdated', JSONobj[boardIndex].slice(1,2)[0]);
		currentCache.set('blacklist', JSONobj[boardIndex].slice(2,3)[0]);
		//console.log('On board ' + JSONobj[boardIndex].slice(0,1)[0]);
		//console.log('Board size: ' + JSONobj[boardIndex].length);
		for(var arrayIndex = 3; arrayIndex < JSONobj[boardIndex].length; arrayIndex += 4)
		{
			//console.log('arrayIndex: ' + arrayIndex);
			currentCache.set(JSONobj[boardIndex].slice(arrayIndex, arrayIndex+1)[0], new Map());
			//console.log('Thread number is ' + JSONobj[boardIndex].slice(arrayIndex, arrayIndex+1)[0]);
			var cachedThread = currentCache.get(JSONobj[boardIndex].slice(arrayIndex, arrayIndex+1)[0]);
			cachedThread.set('files', JSONobj[boardIndex].slice(arrayIndex+1, arrayIndex+2)[0]);
			//console.log('Files: ' + JSON.stringify(cachedThread.get('files')));
			cachedThread.set('externalModified', Number.parseInt((JSONobj[boardIndex][arrayIndex+2])));
			//console.log('externalModified: ' + cachedThread.get('externalModified'));
			cachedThread.set('lastUpdated', Number.parseInt((JSONobj[boardIndex][arrayIndex+3])));
			//console.log('lastUpdated: ' + cachedThread.get('lastUpdated'));
		}
	}
	console.log('Cache imported!');
}

function askAwoo(msg)
{
	console.log(msg.content);
}
function fetchImage(board, amount, msg)
{
	var currCache = boardCache.get(board);
	var threads = new Array();
	for(var val of boardCache.get(board).keys()) if(typeof(val) == 'number') threads.push(val);
	if(threads.length == 0)
	{
		console.log('No threads to pick from, exiting function.');
		return;
	}
	var threadnum = threads[getRandomIntInclusive(0, threads.length-1)];
	var cachedThread = currCache.get(threadnum);
	var threadFiles = cachedThread.get('files');
	var boardBlacklist = currCache.get('blacklist');
	console.log('Chose thread no. ' + threadnum);

	// promise 1
	new Promise((resolve, reject) =>
	{
		let timeSince = (Date.now() - cachedThread.get('lastUpdated'));
		// så länge den cachade tråden är äldre än 5 minuter och yngre än 1 dag uppdateras den
		// är den äldre än 1 dag tas tråden bort
		if(timeSince > 300 && timeSince < 86400)
		{
			console.log('Time since last thread cache: ' + (Date.now() - cachedThread.get('lastUpdated')));
			fetchJSON('http://a.4cdn.org/' + board + '/thread/' + threadnum + '.json', {method:'GET'}).then((thread) =>
			{
				var posts = thread.posts;
				var last_modified = posts[posts.length-1].time;
				if(last_modified == cachedThread.get('externalModified'))
				{
					console.log('Thread not updated since last check, refraining from adding files.');
					resolve();
				} else
				{
					console.log('Thread updated since last check, checking for new files.');
					let post;

					console.log('Iterating post array.');
					for(var postIndex = 0; postIndex < posts.length; postIndex++)
					{
						post = posts[postIndex];
						//console.log("index: " + postIndex);
						//if(post.time == currCache.get('externalModified')) break;
						if(post.tim && post.ext != '.webm') {
							let filename = post.tim.toString() + post.ext.toString();
							//console.log(filename);
							if(boardBlacklist.indexOf(filename) != -1) {
								//console.log('Post no ' + post.no + '\'s file is blacklisted, refraining from adding.');
							} else if(threadFiles.indexOf(filename) == -1)
							{
								//console.log('Post no ' + post.no + ' has a non-duplicate file, adding to new files.');
								threadFiles.push(filename);
							}
						}
					}
					//console.log(boardCache.get(board).get(threadnum).get('files'));
					console.log('Setting new external modified and lastUpdated to cached thread.');
					cachedThread.set('externalModified', last_modified);
					cachedThread.set('lastUpdated', Date.now());
					//console.log(posts);
					resolve();
				}
			}).catch((error) =>
			{
				console.log('Couldn\'t fetch thread.\nError: ' + error);
				return;
			});
		} else
		{
			console.log('Thread cache younger than 5 minutes, not updating.');
			resolve();
		}
	}).then(() =>
	{
		console.log('threadFiles length: ' + threadFiles.length);
		if(threadFiles.length == 0)
		{
			console.log('No files in this thread, deleting and retrying.');
			currCache.delete(threadnum);
			setTimeout(fetchImage, 1000, board, amount, msg);
			return;
		}
		var filecacheIndex = getRandomIntInclusive(0, threadFiles.length-1);
		var filename = threadFiles[filecacheIndex];
		console.log('Chose filename: ' + filename);
		console.log('Deleting filename from cache.');
		threadFiles.splice(filecacheIndex, 1);
		console.log('Adding to blacklist');
		boardBlacklist.unshift(filename);
		if(boardBlacklist.length > 100) boardBlacklist.pop();
		var embed = new Discord.RichEmbed();
		embed.setImage('https://i.4cdn.org/'+ board + '/' + filename);
		embed.setTitle((amount-1).toString() + ' images left.');
		embed.setColor('GREEN');
		msg.channel.send('', {embed}).then((msg) =>
		{
			if(amount > 1)
			{
				setTimeout(fetchImage, 1000, board, amount-1, msg);
			}
		});
	}).catch(() =>
	{
		console.log('Retrying function.');
		setTimeout(fetchImage, 1000, board, amount-1, msg);
	});
}

var commands = global.commands = {
	'awooimg' :
	{
		'desc' : 'Fetches images from 4chan.\nBoards not allowed: [ ' + fourchanBlacklist.toString().replace(/,/g, ' / ') + ' ]',
		'main' : (msg, args) =>
		{
			args = args.split(' ').filter(function(e){return e;});
			if(args.length < 1) throw 'Too few arguments.';
			if(args.length > 2) throw 'Too many arguments.';
			if(fourchanBlacklist.indexOf(args[0]) != -1) throw 'Board doesn\'t exist or isn\'t allowed';
			var board = args[0];
			var amount = 1;
			if(args[1]) amount = parseInt(args[1]);
			console.log('Board: /' + board + '/\nAmount: ' + amount);
			if(!boardCache.has(board))
			{
				console.log('/' + board + '/ does not exist in cache, initialising a Map');
				boardCache.set(board, new Map());
				boardCache.get(board).set('lastUpdated', 0);
				boardCache.get(board).set('blacklist', new Array());
			}

			var currCache = boardCache.get(board);

			// hämta alla trådnummer först, kolla om cachen för brädet är 0
			new Promise((resolve, reject) => {
				// vanlig storlek är 1, om den är större innehåller den skit
				// den uppdaterar också om senaste tiden den uppdaterade var över
				// 60 sekunder sedan
				//console.log('lastupdate: ' + currCache.get('lastUpdated'));
				//console.log('datenow: ' + Date.now());
				if(Date.now() - currCache.get('lastUpdated') > 60)
				{
					// rensa först alla gamla trådar
					// gamla trådar = trådar som inte cachats på mer än 1 dag
					boardCache.get(board).forEach((val, key, map) =>
					{
						try
						{
							//console.log(val);
							if((Date.now() - val.get('lastUpdated')) >= 86400)
							{
								console.log(`Old thread no.${key} found, deleting.`);
								map.delete(key);
							}
						} catch (e)
						{
							//console.log('Error: ' + e.stack);
						}
					});

					console.log('boardCache not updated for 60 seconds, fetching all threads JSON and updating cache.');
					fetchJSON('http://a.4cdn.org/'+board+'/threads.json', {method:'GET'}).then((threads) =>
					{
						//var threads = data;
						//console.log(threads);
						let thread;
						let cachedThread;
						for(var pageN = 0; pageN < threads.length; pageN++) { // iterera genom sidorna
							for(var threadN = 0; threadN < threads[pageN].threads.length; threadN++)
							{
							//console.log("Iterating page " + (pageN+1) + ", thread " + (threadN+1));
								thread = threads[pageN].threads[threadN];
								if(!currCache.get(thread.no))
								{
									currCache.set(thread.no, new Map());
									cachedThread = currCache.get(thread.no);
									cachedThread.set('lastUpdated', Date.now()-301);
									cachedThread.set('externalModified', 0);
									cachedThread.set('files', new Array());
								}
							}
						}

						boardCache.get(board).set('lastUpdated', Date.now());
						resolve();
					});
				} else
				{
					console.log('boardCache is not old.');
					resolve();
				}
			}).then(() => {
				//console.log(boardCache);
				fetchImage(board, amount, msg);
			});
		}
	},
	'killawoo' :
	{
		'desc' : 'Kills awoobot.',
		'perm' : permissions.permLevel.DEV,
		'main' : (msg, args) =>
		{
			msg.channel.send('Awoo...').then((msg) =>
			{
				awoobot.destroy().then(() =>
				{
					console.log('Destroyed awoo, exiting...');
					saveCache();
					permissions.savePerms();
					process.exit(0);
				});
			});
		}
	},
	'fortune' : 
	{
		'desc' : 'Gives you a fortune',
		'main' : (msg, args) =>
		{
			msg.reply('\n```css\nYour fortune: ' + fortunes[getRandomIntInclusive(0, fortunes.length-1)] + '```');
		}
	},
	'awoocmds' :
	{
		'desc' : 'Retrives and prints all commands and gives a brief description of each',
		'main' : (msg, args) =>
		{
			let cmdsIter = Object.keys(commands).sort();
			let totalMessage = '';
			for(let key of cmdsIter)
			{
				totalMessage += '`!' + key + ':`';
				totalMessage += '\n\tDescription: `' + commands[key].desc + '`\n\n';
			}
			msg.reply('\n' + totalMessage);
		}
	},
	'play' :
	{
		'desc' : 'Does stuff lmao',
		'main' : (msg, args) =>
		{
			let filename = args;
			try 
			{
				fs.readFileSync(args);
			} catch(e)
			{
				msg.channel.send('File could not be found, are you sure you wrote it correctly?');
				return;
			}
			playQueue(msg, filename);
		}
	},
	'leavevoice' :
	{
		'main' : (msg, args) =>
		{
			if(!voiceConnection) return;
			voiceConnection.channel.leave();
			voiceConnection = undefined;
			if(voiceDispatcher) voiceDispatcher = undefined;
		}
	},
	'playtime' :
	{
		'main' : (msg, args) =>
		{
			if(!voiceDispatcher)
			{
				msg.channel.send('I\'m not playing anything right now!');
				return;
			}
			msg.channel.send('Current playtime is: ' + voiceDispatcher.time/1000 + ' seconds');
		}
	},
	'quote' :
	{
		'main' : (msg, args) =>
		{
			msg.channel.fetchMessages({limit: 100})
				.then(messages => {
					var userMessages = messages.filterArray(message => {
						var userPass = (message.author.id == msg.author.id);
						var filterPass = !(message.content.startsWith('Momiji') || message.content.startsWith('!') || message.content.startsWith('$'));
						console.log('Parsing message: ' + message.content + '\nFilter pass: ' + filterPass + '\nUser pass: ' + userPass);
						console.log('Returning: ' + (userPass && filterPass));
						return userPass && filterPass;
					}); //returns array
					if(userMessages.length <= 0) throw new Error('No message found!');
					console.log('User messages length: ' + userMessages.length);
					var message = userMessages[getRandomIntInclusive(0, userMessages.length-1)];
					console.log('Sending user message: ' + message.content);
					msg.channel.send('```' + message.content + '```\n\tt. <@!' + message.author.id + '>');
				})
				.catch(console.error);
		}
	}
};

// ladda token från en fil
console.log('Trying to read token from file.');
var token = '';
try
{
	token = fs.readFileSync('token', 'utf8');
	console.log('Successfully read token.');
} catch(err)
{
	console.log('Error: \n' + err.stack);
	process.exit(0);
}

Object.assign(commands, permissions.commands);
permissions.loadPerms();
loadCache();

function checkCmd(msg)
{
	var cmd = msg.content.split(' ')[0].substr(1);
	var args = msg.content.substr(cmd.length+2).trim();
	var cmdObj = commands[cmd];
	if(cmdObj)
	{
		try
		{
			let objPerm = cmdObj.perm || permissions.permLevel.LOW;
			if(!permissions.testPerms(objPerm, msg.author.id)) throw 'You do not have the necessary permissions.';
			console.log('Attempting to execute command: !' + cmd + '\nArgument: ' + args);
			cmdObj.main(msg, args);
		} catch(err)
		{
			msg.channel.send('`Failed to execute command: ' + err + '`');
		}
	}
}

function askMomiji(msg)
{
	var question = msg.content.replace('Momiji ', '');
	console.log('I was asked the following question: ' + question);
	if(question.includes('eller'))
	{
		console.log('The question was a choice question!');
		var choices = question.split('eller');
		var answer = choices[getRandomIntInclusive(0, choices.length-1)];
		msg.reply('```http\n' + answer.trim() + '```');
	} else
	{
		console.log('The question was a yes/no/maybe question!');
		msg.reply('```http\n' + answersList[getRandomIntInclusive(0, answersList.length-1)] + '```');
	}
}

awoobot.on('ready', () =>
{
	console.log('I\'m up!');
});

awoobot.on('message', msg =>
{
	if(msg.author.bot) return; // filtrera bottar
	//if(msg.editable) wordFilter(msg);
	if(msg.content.startsWith('!')) checkCmd(msg);
	if(msg.content.startsWith('Momiji ')) askMomiji(msg);

	if(msg.content == 'lmao')
	{
		msg.delete()
			.then(msg => {
				msg.channel.send('lmao');
			})
			.catch(console.error);
	}
});

awoobot.on('messageUpdate', (oldmsg, newmsg) =>
{
	if((oldmsg.content == newmsg.content) || (newmsg.author.bot)) return;
	//if(newmsg.editable) wordFilter(newmsg);
	if(newmsg.isMentioned(awoobot.user)) askAwoo(newmsg);
	if(newmsg.content.startsWith('!')) checkCmd(newmsg);
	if(newmsg.content.startsWith('Momiji ')) askMomiji(newmsg);
});

awoobot.on('disconnect', () =>
{
	console.log('Disconnected, exiting...');
	saveCache();
	permissions.savePerms();
	console.log('Sleep tight, awoo.');
	process.exit(1);
});

awoobot.on('error', (err) =>
{
	console.log('Awoobot has encountered an error:\n' + err.stack());
	console.log('Destroying client and exiting...\n');
	awoobot.destroy().then(() =>
	{
		console.log('Destroyed awoo, exiting...');
		saveCache();
		permissions.savePerms();
		process.exit(0);
	});
});

awoobot.login(token);
